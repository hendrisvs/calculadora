def division(a, b):
    """Funcion division"""
    if b == 0:
        return ("No se puede dividir entre cero")
    return (a/b)

def suma(a,b,c):
    """Funcion suma"""	
    return (a+b+c)

def resta(a,b):
    """funcion resta"""
    return (a-b)

def multiplica(a,b):
    """funcion multiplicacion"""
    return(a*b)

num1 = 5
num2 = 3
num3 = 12

print(suma(num1, num2, num3))
print(resta(num1, num2))
print(multiplica(num1,num2))
print(division(num1, num2))

print("calculadora")
